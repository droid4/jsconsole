# JS Console

> Simple Javascript console app.


Live demo app available on Amazon App Store:
* [JS Console on Amazon app store](https://www.amazon.com/dp/B075M7YQ4S/)



## How to Use the App

JS Console is a very simple app that lets you edit and run a quick Javascript code on a mobile device. Currently, it provides no other functions.

* Note: This app is intended for developers. You need to have basic knowledge of Javascript programming.

### Getting Started with JS Console

1. JS Console comprises a single screen at the moment.
1. You can just type, or copy and paste, a Javascript code into the code editor at the bottom of the screen.
1. Optionally, you can load an existing code from a file (e.g., on an sdcard) using the "Load Script" button. Note that if there is already a code in the editor, it will be overwritten.
1. Press the "Run Script" button to run the code.
1. If the code runs successfully, then its result will be shown at the top of the screen.
1. The "result" is the value of the last expression/statement in the code.
1. If the result value is an object or an array type, it does not do any conversion. For readability, you may add JSON.stringfy() in your last statement, for instance.



## How to Build

    gradle build


(Before building you may need to comment out the signing command in the gradle build file, or at least add your own signing configs.)



## References

* [J2V8: Java Bindings for V8](https://github.com/eclipsesource/j2v8)
* [Node on Android: Make Node.JS apps for Android](https://github.com/node-on-mobile/node-on-android)
* [LiquidCore: Javascript-based native mobile micro-app development environment](https://github.com/LiquidPlayer/LiquidCore)
* [AndroidJSCore: AndroidJSCore allows Android developers to use JavaScript natively in their apps](https://github.com/ericwlange/AndroidJSCore)
* [V8 JavaScript Engine](https://android.googlesource.com/platform/external/v8)
* [Android Copy and Paste](https://developer.android.com/guide/topics/text/copy-paste.html)



