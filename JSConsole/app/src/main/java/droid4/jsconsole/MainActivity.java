
package droid4.jsconsole;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.eclipsesource.v8.V8;
import com.eclipsesource.v8.V8Array;
import com.eclipsesource.v8.V8Object;
import com.eclipsesource.v8.V8Value;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;

import droid4.jsconsole.util.FilePathUtil;

public class MainActivity extends AppCompatActivity {
    private static String LOG_TAG = MainActivity.class.getName();
    private static int REQUEST_SCRIPT_FILE_PICK = 1001;

    // Instance cache
    private V8 v8Runtime = null;


    // tbd:
    // pause/resume vs stop/start ????

    @Override
    protected void onResume() {
        super.onResume();

        // Testing...
        v8Runtime = V8.createV8Runtime();
        android.util.Log.d(LOG_TAG, "V8 created: " + v8Runtime.toString());
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (v8Runtime != null && !v8Runtime.isReleased()) {
            v8Runtime.release();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

//        if (v8Runtime != null && !v8Runtime.isReleased()) {
//            v8Runtime.release();
//        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        // Testing...
//        v8Runtime = V8.createV8Runtime();
//        android.util.Log.d(LOG_TAG, "V8 created: " + v8Runtime.toString());

        Button buttonRunScript = (Button) findViewById(R.id.button_run_script);
        buttonRunScript.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.util.Log.d(LOG_TAG, "buttonRunScript Button clicked.");

                EditText edittextResult = (EditText) findViewById(R.id.edittext_script_result);
                edittextResult.setText("");

                // This does not work.
//                NodeJS nodeJS = NodeJS.createNodeJS();
//                android.util.Log.d(LOG_TAG, "NodeJS created: " + nodeJS.toString());

                // temporary
//                String script = "var a = 1; var b = a + 3; b;";
//                int result = v8Runtime.executeIntegerScript(script);
//                android.util.Log.d(LOG_TAG, "result = " + result);


                String output = "";
                try {
                    // tbd:
                    // Load script from a file ???

                    EditText edittextInput = (EditText) findViewById(R.id.edittext_input_script);
                    String inputScript = edittextInput.getText().toString();
                    android.util.Log.d(LOG_TAG, "inputScript = " + inputScript);

                    // TBD: This will crash if the script is invalid....
                    Object resObj = null;
                    if (v8Runtime != null && !v8Runtime.isReleased()) {
                        resObj = v8Runtime.executeScript(inputScript);
                    }
                    android.util.Log.d(LOG_TAG, "resObj = " + resObj);

                    output = (resObj == null
                            // || ((resObj instanceof Integer) && (((Integer) resObj) == V8Object.UNDEFINED))  // ??? This does not work...
                    )
                            ? ""
                            : resObj.toString();  // tbd: hash, array, etc. ????

                    output = "";
                    if (resObj == null) { // or == undefined ???
                        // ""
                    } else {
                        if (resObj instanceof V8Value) {
                            if (resObj instanceof V8Array) {
                                android.util.Log.d(LOG_TAG, "resObj is V8Array.");
                                output = "[Array]";
                            } else if (resObj instanceof V8Object) {
                                android.util.Log.d(LOG_TAG, "resObj is V8Object.");
                                output = "[Object]";
                            } else {
                                android.util.Log.d(LOG_TAG, "resObj is neither V8Array no V8Object.");
                                output = "[?]";   // ???
                            }
                        } else {
                            //  ???
                            android.util.Log.d(LOG_TAG, "resObj is not V8Value.");
                            output = resObj.toString();   // ?
                        }
                    }

                } catch (Exception ex) {
                    android.util.Log.e(LOG_TAG, "ex = " + ex.getMessage());
                    Toast.makeText(view.getContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
                }
                edittextResult.setText(output);

                // tbd:
                // Copy output to clipboard?
                // ...

            }
        });


        Button buttonLoadScript = (Button) findViewById(R.id.button_load_script);
        buttonLoadScript.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.util.Log.d(LOG_TAG, "buttonLoadScript Button clicked.");

                EditText edittextResult = (EditText) findViewById(R.id.edittext_script_result);
                edittextResult.setText("");

                EditText edittextInput = (EditText) findViewById(R.id.edittext_input_script);
                String inputScript = edittextInput.getText().toString();
                android.util.Log.d(LOG_TAG, "Current inputScript = " + inputScript);

                if (TextUtils.isEmpty(inputScript)) {
                    loadNewScript();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setTitle(R.string.query_dialog_already_have_script)
                            .setMessage(R.string.query_dialog_script_overwrite_question)
                            .setPositiveButton(R.string.overwrite_script, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    loadNewScript();
                                }
                            })
                            .setNegativeButton(R.string.cancel_query_dialog, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // Just close the dialog.
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }

            }
        });

    }

    private void loadNewScript() {
        android.util.Log.d(LOG_TAG, "loadNewScript() called.");

        EditText edittextResult = (EditText) findViewById(R.id.edittext_script_result);
        edittextResult.setText("");

        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        String[] mimetypes = {"text/*"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(intent, REQUEST_SCRIPT_FILE_PICK);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SCRIPT_FILE_PICK
                && resultCode == Activity.RESULT_OK && data != null) {
            Uri uri = data.getData();
            if (uri != null) {
                File file = null;
                try {
                    String path = FilePathUtil.getPath(this, uri);
                    android.util.Log.d(LOG_TAG, ">>> Upload file path = " + path);
                    file = new File(path);
                } catch (URISyntaxException e) {
                    android.util.Log.d(LOG_TAG, "exception: " + e);
                }

                if (file != null && file.exists()) {
                    StringBuilder text = new StringBuilder();
                    try {
                        BufferedReader br = new BufferedReader(new FileReader(file));
                        String line;

                        while ((line = br.readLine()) != null) {
                            text.append(line);
                            text.append('\n');
                        }
                        br.close();
                    } catch (IOException e) {
                        android.util.Log.d(LOG_TAG, "exception: " + e);
                    }
                    String inputScript = text.toString();
                    android.util.Log.d(LOG_TAG, "inputScript loaded = " + inputScript);

                    EditText edittextInput = (EditText) findViewById(R.id.edittext_input_script);
                    edittextInput.setText(inputScript);
                } else {
                    // ???
                    android.util.Log.d(LOG_TAG, "File does not exist. uri = " + uri);
                }
            } else {
                // ???
            }
        } else {
            // ...
        }
    }


}
